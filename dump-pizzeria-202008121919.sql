-- MySQL dump 10.13  Distrib 5.7.30, for Win64 (x86_64)
--
-- Host: localhost    Database: pizzeria
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.11-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cliente_table_`
--

DROP TABLE IF EXISTS `cliente_table_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente_table_` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente_table_`
--

LOCK TABLES `cliente_table_` WRITE;
/*!40000 ALTER TABLE `cliente_table_` DISABLE KEYS */;
INSERT INTO `cliente_table_` VALUES (1,'thompson.lew@weissnat.com','Marlin Bartell','2020-08-12 08:08:43','2020-08-12 08:08:43'),(2,'lisa11@hotmail.com','Kylee Reichel I','2020-08-12 08:08:43','2020-08-12 08:08:43'),(3,'alverta.oreilly@yahoo.com','Dr. Broderick Ankunding','2020-08-12 08:08:44','2020-08-12 08:08:44'),(4,'ziemann.santiago@gmail.com','Miss Dovie Howe','2020-08-12 08:08:44','2020-08-12 08:08:44'),(5,'littel.liliane@yahoo.com','Monserrate Carroll','2020-08-12 08:08:44','2020-08-12 08:08:44');
/*!40000 ALTER TABLE `cliente_table_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_pedido_table_`
--

DROP TABLE IF EXISTS `detalle_pedido_table_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_pedido_table_` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_pedido` bigint(20) unsigned NOT NULL,
  `id_producto` bigint(20) unsigned NOT NULL,
  `cantidad` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `detalle_pedido_table__id_pedido_foreign` (`id_pedido`),
  KEY `detalle_pedido_table__id_producto_foreign` (`id_producto`),
  CONSTRAINT `detalle_pedido_table__id_pedido_foreign` FOREIGN KEY (`id_pedido`) REFERENCES `pedido_table_` (`id`),
  CONSTRAINT `detalle_pedido_table__id_producto_foreign` FOREIGN KEY (`id_producto`) REFERENCES `producto_table_` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_pedido_table_`
--

LOCK TABLES `detalle_pedido_table_` WRITE;
/*!40000 ALTER TABLE `detalle_pedido_table_` DISABLE KEYS */;
INSERT INTO `detalle_pedido_table_` VALUES (1,5,8,68,'2020-08-12 08:08:46','2020-08-12 08:08:46'),(2,4,5,34,'2020-08-12 08:08:46','2020-08-12 08:08:46'),(3,4,2,98,'2020-08-12 08:08:46','2020-08-12 08:08:46'),(4,4,6,64,'2020-08-12 08:08:46','2020-08-12 08:08:46'),(5,4,4,94,'2020-08-12 08:08:47','2020-08-12 08:08:47'),(6,12,1,3,'2020-08-13 02:29:29','2020-08-13 02:29:29'),(7,12,2,2,'2020-08-13 02:29:29','2020-08-13 02:29:29'),(8,13,1,3,'2020-08-13 02:37:43','2020-08-13 02:37:43'),(9,13,2,2,'2020-08-13 02:37:43','2020-08-13 02:37:43'),(10,14,1,1,'2020-08-13 02:47:24','2020-08-13 02:47:24'),(11,14,2,2,'2020-08-13 02:47:24','2020-08-13 02:47:24'),(12,15,1,1,'2020-08-13 02:54:05','2020-08-13 02:54:05'),(13,15,2,1,'2020-08-13 02:54:05','2020-08-13 02:54:05'),(14,16,1,1,'2020-08-13 02:55:46','2020-08-13 02:55:46'),(15,17,1,1,'2020-08-13 03:20:05','2020-08-13 03:20:05'),(16,18,1,1,'2020-08-13 03:39:01','2020-08-13 03:39:01'),(17,19,1,1,'2020-08-13 03:45:22','2020-08-13 03:45:22'),(18,20,1,1,'2020-08-13 03:46:17','2020-08-13 03:46:17'),(19,20,2,1,'2020-08-13 03:46:17','2020-08-13 03:46:17'),(20,21,1,1,'2020-08-13 03:49:30','2020-08-13 03:49:30'),(21,21,2,1,'2020-08-13 03:49:30','2020-08-13 03:49:30'),(22,22,1,1,'2020-08-13 03:54:12','2020-08-13 03:54:12'),(23,22,2,1,'2020-08-13 03:54:13','2020-08-13 03:54:13'),(24,23,1,1,'2020-08-13 03:55:16','2020-08-13 03:55:16'),(25,23,2,1,'2020-08-13 03:55:16','2020-08-13 03:55:16'),(26,24,1,1,'2020-08-13 04:01:08','2020-08-13 04:01:08'),(27,24,2,1,'2020-08-13 04:01:08','2020-08-13 04:01:08'),(28,25,1,1,'2020-08-13 04:01:59','2020-08-13 04:01:59'),(29,25,1,1,'2020-08-13 04:01:59','2020-08-13 04:01:59'),(30,26,1,1,'2020-08-13 04:06:09','2020-08-13 04:06:09'),(31,26,1,1,'2020-08-13 04:06:09','2020-08-13 04:06:09'),(32,27,1,1,'2020-08-13 04:12:28','2020-08-13 04:12:28'),(33,27,1,1,'2020-08-13 04:12:28','2020-08-13 04:12:28'),(34,28,1,1,'2020-08-13 04:13:29','2020-08-13 04:13:29'),(35,28,2,1,'2020-08-13 04:13:29','2020-08-13 04:13:29'),(36,29,1,1,'2020-08-13 04:14:11','2020-08-13 04:14:11'),(37,29,2,2,'2020-08-13 04:14:11','2020-08-13 04:14:11'),(38,30,1,1,'2020-08-13 04:23:47','2020-08-13 04:23:47'),(39,30,2,2,'2020-08-13 04:23:47','2020-08-13 04:23:47'),(40,31,1,1,'2020-08-13 04:29:32','2020-08-13 04:29:32'),(41,31,2,2,'2020-08-13 04:29:32','2020-08-13 04:29:32'),(42,32,1,1,'2020-08-13 04:31:20','2020-08-13 04:31:20'),(43,32,2,2,'2020-08-13 04:31:20','2020-08-13 04:31:20'),(44,33,1,3,'2020-08-13 04:31:43','2020-08-13 04:31:43'),(45,33,2,2,'2020-08-13 04:31:43','2020-08-13 04:31:43'),(46,34,1,1,'2020-08-13 04:40:10','2020-08-13 04:40:10'),(47,34,2,2,'2020-08-13 04:40:10','2020-08-13 04:40:10'),(48,35,1,1,'2020-08-13 07:11:17','2020-08-13 07:11:17'),(49,35,2,2,'2020-08-13 07:11:17','2020-08-13 07:11:17'),(50,36,1,1,'2020-08-13 07:13:37','2020-08-13 07:13:37'),(51,36,2,2,'2020-08-13 07:13:37','2020-08-13 07:13:37'),(52,37,1,1,'2020-08-13 07:16:32','2020-08-13 07:16:32'),(53,37,2,2,'2020-08-13 07:16:32','2020-08-13 07:16:32');
/*!40000 ALTER TABLE `detalle_pedido_table_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingrediente_table_`
--

DROP TABLE IF EXISTS `ingrediente_table_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingrediente_table_` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `precio` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingrediente_table_`
--

LOCK TABLES `ingrediente_table_` WRITE;
/*!40000 ALTER TABLE `ingrediente_table_` DISABLE KEYS */;
INSERT INTO `ingrediente_table_` VALUES (1,'Peperoni',2,'2020-08-12 08:08:44','2020-08-12 08:08:44'),(2,'Queso',1,'2020-08-12 08:08:45','2020-08-12 08:08:45'),(3,'Jamon',2,'2020-08-12 08:08:45','2020-08-12 08:08:45'),(4,'Piña',1,'2020-08-12 08:08:45','2020-08-12 08:08:45'),(5,'Hongo',2,'2020-08-12 08:08:45','2020-08-12 08:08:45'),(6,'Carne',4,'2020-08-12 08:08:45','2020-08-12 08:08:45'),(7,'Tocino',2,'2020-08-12 08:08:45','2020-08-12 08:08:45'),(8,'Cebolla',1,'2020-08-12 08:08:45','2020-08-12 08:08:45'),(9,'Chile verde',1,'2020-08-12 08:08:45','2020-08-12 08:08:45'),(10,'Aceitunas',1,'2020-08-12 08:08:45','2020-08-12 08:08:45'),(11,'Salsa extra',2,'2020-08-12 08:08:45','2020-08-12 08:08:45');
/*!40000 ALTER TABLE `ingrediente_table_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=289 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (280,'2014_10_12_000000_create_users_table',1),(281,'2019_08_19_000000_create_failed_jobs_table',1),(282,'2020_08_11_045408_create_cliente_table_',1),(283,'2020_08_11_052851_create_sucursal_table_',1),(284,'2020_08_11_053550_create_pedido_table_',1),(285,'2020_08_11_053610_create_detalle_pedido_table_',1),(286,'2020_08_11_053643_create_ingrediente_table_',1),(287,'2020_08_11_053717_create_pizza_prede_table_',1),(288,'2020_08_11_053732_create_pizza_perso_table_',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_table_`
--

DROP TABLE IF EXISTS `pedido_table_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_table_` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_cliente` bigint(20) unsigned NOT NULL,
  `id_sucursal` bigint(20) unsigned NOT NULL,
  `total` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pedido_table__id_cliente_foreign` (`id_cliente`),
  KEY `pedido_table__id_sucursal_foreign` (`id_sucursal`),
  CONSTRAINT `pedido_table__id_cliente_foreign` FOREIGN KEY (`id_cliente`) REFERENCES `cliente_table_` (`id`),
  CONSTRAINT `pedido_table__id_sucursal_foreign` FOREIGN KEY (`id_sucursal`) REFERENCES `sucursal_table_` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_table_`
--

LOCK TABLES `pedido_table_` WRITE;
/*!40000 ALTER TABLE `pedido_table_` DISABLE KEYS */;
INSERT INTO `pedido_table_` VALUES (1,4,4,106,'2020-08-12 08:08:44','2020-08-12 08:08:44'),(2,2,4,228,'2020-08-12 08:08:44','2020-08-12 08:08:44'),(3,1,3,212,'2020-08-12 08:08:44','2020-08-12 08:08:44'),(4,4,3,945,'2020-08-12 08:08:44','2020-08-12 08:08:44'),(5,5,1,894,'2020-08-12 08:08:44','2020-08-12 08:08:44'),(6,1,1,12,'2020-08-13 00:19:09','2020-08-13 00:19:09'),(7,1,1,34,'2020-08-13 00:44:12','2020-08-13 00:44:12'),(8,1,1,34,'2020-08-13 02:15:01','2020-08-13 02:15:01'),(9,1,1,50,'2020-08-13 02:22:38','2020-08-13 02:22:38'),(10,1,1,50,'2020-08-13 02:27:59','2020-08-13 02:27:59'),(11,1,1,50,'2020-08-13 02:28:18','2020-08-13 02:28:18'),(12,1,1,50,'2020-08-13 02:29:29','2020-08-13 02:29:29'),(13,1,1,100,'2020-08-13 02:37:43','2020-08-13 02:37:43'),(14,1,1,107,'2020-08-13 02:47:24','2020-08-13 02:47:24'),(15,1,1,63,'2020-08-13 02:54:04','2020-08-13 02:54:04'),(16,1,1,19,'2020-08-13 02:55:46','2020-08-13 02:55:46'),(17,1,1,20,'2020-08-13 03:20:05','2020-08-13 03:20:05'),(18,1,1,20,'2020-08-13 03:39:01','2020-08-13 03:39:01'),(19,1,1,11,'2020-08-13 03:45:22','2020-08-13 03:45:22'),(20,1,1,32,'2020-08-13 03:46:17','2020-08-13 03:46:17'),(21,1,1,79,'2020-08-13 03:49:30','2020-08-13 03:49:30'),(22,1,1,100,'2020-08-13 03:54:12','2020-08-13 03:54:12'),(23,1,1,43,'2020-08-13 03:55:16','2020-08-13 03:55:16'),(24,1,1,32,'2020-08-13 04:01:08','2020-08-13 04:01:08'),(25,1,1,25,'2020-08-13 04:01:59','2020-08-13 04:01:59'),(26,1,1,33,'2020-08-13 04:06:09','2020-08-13 04:06:09'),(27,1,1,22,'2020-08-13 04:12:28','2020-08-13 04:12:28'),(28,1,1,27,'2020-08-13 04:13:29','2020-08-13 04:13:29'),(29,1,1,40,'2020-08-13 04:14:11','2020-08-13 04:14:11'),(30,1,1,43,'2020-08-13 04:23:47','2020-08-13 04:23:47'),(31,1,1,32,'2020-08-13 04:29:31','2020-08-13 04:29:31'),(32,1,1,43,'2020-08-13 04:31:20','2020-08-13 04:31:20'),(33,1,1,50,'2020-08-13 04:31:43','2020-08-13 04:31:43'),(34,1,1,34,'2020-08-13 04:40:10','2020-08-13 04:40:10'),(35,1,1,43,'2020-08-13 07:11:17','2020-08-13 07:11:17'),(36,1,1,43,'2020-08-13 07:13:37','2020-08-13 07:13:37'),(37,1,1,43,'2020-08-13 07:16:32','2020-08-13 07:16:32');
/*!40000 ALTER TABLE `pedido_table_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pizza_perso_table_`
--

DROP TABLE IF EXISTS `pizza_perso_table_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pizza_perso_table_` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_detalle_pedido` bigint(20) unsigned NOT NULL,
  `id_ingrediente` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pizza_perso_table__id_detalle_pedido_foreign` (`id_detalle_pedido`),
  KEY `pizza_perso_table__id_ingrediente_foreign` (`id_ingrediente`),
  CONSTRAINT `pizza_perso_table__id_detalle_pedido_foreign` FOREIGN KEY (`id_detalle_pedido`) REFERENCES `detalle_pedido_table_` (`id`),
  CONSTRAINT `pizza_perso_table__id_ingrediente_foreign` FOREIGN KEY (`id_ingrediente`) REFERENCES `ingrediente_table_` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pizza_perso_table_`
--

LOCK TABLES `pizza_perso_table_` WRITE;
/*!40000 ALTER TABLE `pizza_perso_table_` DISABLE KEYS */;
INSERT INTO `pizza_perso_table_` VALUES (1,5,7,'2020-08-12 08:08:47','2020-08-12 08:08:47'),(2,5,8,'2020-08-12 08:08:47','2020-08-12 08:08:47'),(3,3,10,'2020-08-12 08:08:47','2020-08-12 08:08:47'),(4,4,4,'2020-08-12 08:08:47','2020-08-12 08:08:47'),(5,1,6,'2020-08-12 08:08:47','2020-08-12 08:08:47');
/*!40000 ALTER TABLE `pizza_perso_table_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pizza_prede_table_`
--

DROP TABLE IF EXISTS `pizza_prede_table_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pizza_prede_table_` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_producto` bigint(20) unsigned NOT NULL,
  `id_ingrediente` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pizza_prede_table__id_producto_foreign` (`id_producto`),
  KEY `pizza_prede_table__id_ingrediente_foreign` (`id_ingrediente`),
  CONSTRAINT `pizza_prede_table__id_ingrediente_foreign` FOREIGN KEY (`id_ingrediente`) REFERENCES `ingrediente_table_` (`id`),
  CONSTRAINT `pizza_prede_table__id_producto_foreign` FOREIGN KEY (`id_producto`) REFERENCES `producto_table_` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pizza_prede_table_`
--

LOCK TABLES `pizza_prede_table_` WRITE;
/*!40000 ALTER TABLE `pizza_prede_table_` DISABLE KEYS */;
INSERT INTO `pizza_prede_table_` VALUES (1,2,2,'2020-08-12 08:08:47','2020-08-12 08:08:47'),(2,10,2,'2020-08-12 08:08:47','2020-08-12 08:08:47'),(3,7,10,'2020-08-12 08:08:47','2020-08-12 08:08:47'),(4,5,4,'2020-08-12 08:08:47','2020-08-12 08:08:47'),(5,8,4,'2020-08-12 08:08:47','2020-08-12 08:08:47');
/*!40000 ALTER TABLE `pizza_prede_table_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto_table_`
--

DROP TABLE IF EXISTS `producto_table_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto_table_` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `precio` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto_table_`
--

LOCK TABLES `producto_table_` WRITE;
/*!40000 ALTER TABLE `producto_table_` DISABLE KEYS */;
INSERT INTO `producto_table_` VALUES (1,'Personal de la casa',8,'2020-08-12 08:08:45','2020-08-12 08:08:45'),(2,'Familiar de la casa',13,'2020-08-12 08:08:45','2020-08-12 08:08:45'),(3,'Gigante de la casa',18,'2020-08-12 08:08:45','2020-08-12 08:08:45'),(4,'Personal hawaiana',7,'2020-08-12 08:08:46','2020-08-12 08:08:46'),(5,'Familiar hawaiana',12,'2020-08-12 08:08:46','2020-08-12 08:08:46'),(6,'Gigante hawaiana',17,'2020-08-12 08:08:46','2020-08-12 08:08:46'),(7,'Personal suprema',9,'2020-08-12 08:08:46','2020-08-12 08:08:46'),(8,'Familiar suprema',14,'2020-08-12 08:08:46','2020-08-12 08:08:46'),(9,'Gigante suprema',21,'2020-08-12 08:08:46','2020-08-12 08:08:46'),(10,'Personal Personalizada',5,'2020-08-12 08:08:46','2020-08-12 08:08:46'),(11,'Familiar Personalizada',8,'2020-08-12 08:08:46','2020-08-12 08:08:46'),(12,'Gigante Personalizada',12,'2020-08-12 08:08:46','2020-08-12 08:08:46');
/*!40000 ALTER TABLE `producto_table_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sucursal_table_`
--

DROP TABLE IF EXISTS `sucursal_table_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sucursal_table_` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sucursal_table_`
--

LOCK TABLES `sucursal_table_` WRITE;
/*!40000 ALTER TABLE `sucursal_table_` DISABLE KEYS */;
INSERT INTO `sucursal_table_` VALUES (1,'South Kyleeview','94815 Savanah Landing\nWest Jeffry, CT 81651-6278','2020-08-12 08:08:44','2020-08-12 08:08:44'),(2,'Jaydaborough','862 Cummings River Apt. 560\nYundtmouth, OH 27229','2020-08-12 08:08:44','2020-08-12 08:08:44'),(3,'West Easton','52804 Madyson Crossroad\nWest Noratown, AZ 04144-2140','2020-08-12 08:08:44','2020-08-12 08:08:44'),(4,'Lilianaburgh','82336 Pat Flat Suite 109\nRomanstad, PA 52523','2020-08-12 08:08:44','2020-08-12 08:08:44'),(5,'South Kayli','3016 Renner Alley\nSouth Alainafurt, RI 09023','2020-08-12 08:08:44','2020-08-12 08:08:44');
/*!40000 ALTER TABLE `sucursal_table_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'pizzeria'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-12 19:19:53
