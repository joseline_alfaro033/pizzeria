<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    protected $table='sucursal_table_';
    protected $primaryKey = 'id';
    protected $fillable = array('nombre','direccion');
    protected $hidden = ['created_at','updated_at']; 

    public function pedidos()
	{	
		return $this->hasMany('App\Pedido');
	}
}
