<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PizzaPerso extends Model
{
    protected $table='pizza_perso_table_';
    protected $primaryKey = 'id';
    protected $fillable = array('id_detalle_pedido','id_ingrediente');
    protected $hidden = ['created_at','updated_at']; 

  
    public function detallePedidos()
	{	
		return $this->hasMany('App\DetallePedido');
    }
    public function ingredientes()
	{	
		return $this->hasMany('App\Ingrediente');
	}
}
