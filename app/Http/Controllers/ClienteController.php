<?php

namespace App\Http\Controllers;
use App\Cliente;
use App\Pedido;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    public function index(){
        $clientes = Cliente::all();
        return $clientes;
    }

public function historial($id_cliente){
   $listaPedidos = Pedido::with('detallePedidos')->where('id_cliente',$id_cliente)->get();
    return $listaPedidos;
}

public function clienteFrecuente(){
    
    $clienteF = DB::table('pedido_table_')->select('id_cliente','cliente_table_.nombre', DB::raw('count(*) as total'))->join('cliente_table_','cliente_table_.id','=','pedido_table_.id_cliente')->groupBy('pedido_table_.id_cliente','cliente_table_.nombre')->take(10)->get(); 
    return $clienteF;
}

public function clienteMasGasta(){
    $clienteMG = DB::table('pedido_table_')->select('id_cliente','cliente_table_.nombre', DB::raw('CONCAT("$ ",SUM(total)) as total'))->join('cliente_table_','cliente_table_.id','=','pedido_table_.id_cliente')->groupBy('pedido_table_.id_cliente','cliente_table_.nombre')->take(10)->get(); 
    return $clienteMG;
}

    public function store(Request $request){
        $cliente = Cliente::create($request->all());
        return $cliente;
    }
}
