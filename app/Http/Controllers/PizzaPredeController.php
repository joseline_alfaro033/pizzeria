<?php

namespace App\Http\Controllers;
use App\Producto;
use Illuminate\Http\Request;

class PizzaPredeController extends Controller
{
    public function index(){
        $pizzaPredes = Producto::with('ingredientes')->get();
     
        return $pizzaPredes;
    }

    
}
