<?php

namespace App\Http\Controllers;
use App\Sucursal;
use Illuminate\Http\Request;

class SucursalController extends Controller
{
    public function index(){
        $sucursales = Sucursal::all();
        return $sucursales;
    }

    public function store(Request $request){
        $sucursal = Sucursal::create($request->all());
        return $sucursal;
    }
}
