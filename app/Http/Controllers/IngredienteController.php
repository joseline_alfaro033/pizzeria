<?php
namespace App\Http\Controllers;

use App\Ingrediente;

use Illuminate\Http\Request;

class IngredienteController extends Controller
{
    public function index(){
        $ingredientes = Ingrediente::all();
        return $ingredientes;
    }

    public function store(Request $request){
        $ingrediente = Ingrediente::create($request->all());
        return $ingrediente;
    }
}
