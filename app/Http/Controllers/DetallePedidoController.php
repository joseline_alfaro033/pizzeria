<?php

namespace App\Http\Controllers;
use App\DetallePedido;
use Illuminate\Http\Request;

class DetallePedidoController extends Controller
{
    public function index(){
        $detallePedidos = DetallePedido::all();
        return $detallePedidos;
    }

    public function store(Request $request){
        $detallePedido = DetallePedido::create($request->all());
        return $detallePedido;
    }
}
