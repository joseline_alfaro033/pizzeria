<?php

namespace App\Http\Controllers;
use App\Pedido;
use App\Producto;
use App\Ingrediente;
use App\DetallePedido;

use Illuminate\Http\Request;

class PedidoController extends Controller
{
    public function index(){
        $pedidos = Pedido::with('detallePedidos','cliente')->get();
        return $pedidos;
    }

    public function almacenarPedido(Request $request){

        $pedido=$request->get('pedido');
        $detallePedido=$pedido['detallePedido'];
        $idsucursal=$pedido['sucursal'];
        $idcliente=$pedido['cliente'];
        $total = 0;
     
    //Almacenar pedido
    $pedido = new Pedido();
    $pedido->total=$this->calcularTotal($detallePedido);
    $pedido->id_sucursal=$idsucursal;
    $pedido->id_cliente=$idcliente;

    if($pedido->save()){
     
        $this->almacenarDetalle($detallePedido, $pedido->id);
        $pedidos = Pedido::with('detallePedidos.productos','cliente')->where('id',$pedido->id)->get();
        return $pedidos;
    }else{

        $mensaje = "{'msj':'No se guardó pedido'}";
        return $mensaje;
    }

        
    }

    public function calcularTotal($detallePedido){
        $total =0;
        
      
        foreach( $detallePedido as $detalle ){
            $prod=$detalle['prod'];
            $cantidad=$detalle['cantidad'];
        
            $precioPro = Producto::where("id","=",$prod['id'])->select("precio")->first();
           
            $precioProducto=$precioPro['precio'];
            
            if(isset($prod['ingredientes'])){
                $ingredientes=$prod['ingredientes'];
                $total1=0;
                $total2=0;
                foreach( $ingredientes as $ingrediente ){
                   $precioIngre = Ingrediente::where("id","=",$ingrediente['id'])->select("precio")->first();
                   $total1 += ($precioIngre['precio']);
                 }
                 
                
                $precioProducto =+ $total1+$precioPro['precio'];
            }

                $total += ( $precioProducto* $cantidad); 
           
            
           
        }
        return $total;
    }

    public function almacenarDetalle($detallePedido,$idpedido){
        foreach( $detallePedido as $detalle ){
            $prod=$detalle['prod'];
            $cantidad=$detalle['cantidad'];

            $detalleP = new DetallePedido();
            $detalleP->id_pedido=$idpedido;
            $detalleP->id_producto=$prod['id'];
            $detalleP->cantidad=$cantidad;
            $detalleP->save();

        }
    }

      

}
