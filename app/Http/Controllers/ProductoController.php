<?php

namespace App\Http\Controllers;
use App\Producto;
use Illuminate\Http\Request;

class ProductoController extends Controller
{
    public function index(){
        $productos = Producto::all();
        return $productos;
    }

    public function store(Request $request){
        $producto = Producto::create($request->all());
        return $producto;
    }
}
