<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table='cliente_table_';
    protected $primaryKey = 'id';
    protected $fillable = array('email','nombre');
    protected $hidden = ['created_at','updated_at']; 

    public function pedidos()
	{	
		return $this->hasMany('App\Pedido');
	}
	
}
