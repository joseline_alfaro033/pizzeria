<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detallePedido extends Model
{
    protected $table='detalle_pedido_table_';
    protected $primaryKey = 'id';
    protected $fillable = array('id_pedido','id_producto','cantidad');
    protected $hidden = ['created_at','updated_at']; 

    public function pedidos()
	{	
		return $this->belongsTo('App\Pedido');
    }
    public function productos()
	{	
		return $this->hasOne('App\Producto','id','id_producto');
    }
    public function pizzaPerso()
	{	
		return $this->belongsTo('App\PizzaPerso');
	}

}
