<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PizzaPrede extends Model
{
    protected $table='pizza_prede_table_';
    protected $primaryKey = 'id';
    protected $fillable = array('id_producto','id_ingrediente');
    protected $hidden = ['created_at','updated_at']; 

  
    public function productos()
	{	
		return $this->hasMany('App\Producto');
    }
    public function ingredientes()
	{	
		return $this->hasMany('App\Ingrediente');
	}
}
