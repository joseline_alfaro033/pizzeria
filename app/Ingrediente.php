<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingrediente extends Model
{
    protected $table='ingrediente_table_';
    protected $primaryKey = 'id';
    protected $fillable = array('nombre','precio');
    protected $hidden = ['created_at','updated_at']; 

    public function productos()
	{	
		return $this->hasMany('App\Producto');
    }
    public function pizzaPerso()
	{	
		return $this->hasMany('App\PizzaPerso');
    }
    public function pizzaPrede()
	{	
		return $this->hasMany('App\PizzaPrede');
	}
}
