<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $table='pedido_table_';
    protected $primaryKey = 'id';
    protected $fillable = array('id_sucursal','total');
    protected $hidden = ['created_at','updated_at']; 

    public function cliente()
	{	
		return $this->hasOne('App\Cliente','id','id_cliente');
    }
    public function sucursales()
	{	
		return $this->belongsTo('App\Sucursal');
    }
    public function detallePedidos()
	{	
		return $this->hasMany('App\DetallePedido', 'id_pedido','id');
	}
}
