<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table='producto_table_';
    protected $primaryKey = 'id';
    protected $fillable = array('nombre','precio');
    protected $hidden = ['created_at','updated_at']; 

    public function detallePedidos()
	{	
		return $this->hasMany('App\DetallePedido');
	}
    public function ingredientes()
	{	
        return $this->belongsToMany('App\Ingrediente','pizza_prede_table_','id_producto','id_ingrediente');
       }
    public function pizzaPrede()
	{	
		return $this->hasMany('App\PizzaPrede');
	}
	
}
