<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
class PizzaPredeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=0; $i < 5; $i++) {
              \DB::table("pizza_prede_table_")->insert(
                    array(
                          'id_producto' => $this->getRandomProductoId(),
                          
                          'id_ingrediente' => $this->getRandomIngredienteId(),
                         
                          'created_at' => date('Y-m-d H:m:s'),
                          'updated_at' => date('Y-m-d H:m:s')
                    )
              );
        }
   
    }
    private function getRandomProductoId() {
        $producto = \App\Producto::inRandomOrder()->first();
        return $producto->id;
    }
  
    private function getRandomIngredienteId() {
        $ingre = \App\Ingrediente::inRandomOrder()->first();
        return $ingre->id;
    }
    
}
