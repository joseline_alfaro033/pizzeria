<?php

use Illuminate\Database\Seeder;
use app\Cliente;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PedidoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
            for ($i=0; $i < 10; $i++) {
                  \DB::table("pedido_table_")->insert(
                        array(
                              'id_cliente' => $this->getRandomClienteId(),
                              
                              'id_sucursal' => $this->getRandomSucursalId(),
                             'total'  => $faker->randomNumber(2),
                              'created_at' => date('Y-m-d H:m:s'),
                              'updated_at' => date('Y-m-d H:m:s')
                        )
                  );
            }
       
        }
        private function getRandomClienteId() {
            $client = \App\Cliente::inRandomOrder()->first();
            return $client->id;
        }
        private function getRandomSucursalId() {
            $sucurs = \App\Sucursal::inRandomOrder()->first();
            return $sucurs->id;
        }
   
    
}
