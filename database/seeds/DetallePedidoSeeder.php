<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
class DetallePedidoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
            for ($i=0; $i < 5; $i++) {
                  \DB::table("detalle_pedido_table_")->insert(
                        array(
                              'id_pedido' => $this->getRandomPedidoId(),
                              
                              'id_producto' => $this->getRandomProductoId(),
                             'cantidad'  => $faker->randomNumber(2),
                              'created_at' => date('Y-m-d H:m:s'),
                              'updated_at' => date('Y-m-d H:m:s')
                        )
                  );
            }
       
        }
        private function getRandomPedidoId() {
            $pedido = \App\Pedido::inRandomOrder()->first();
            return $pedido->id;
        }
        private function getRandomProductoId() {
            $producto = \App\Producto::inRandomOrder()->first();
            return $producto->id;
        }
   
    
}
