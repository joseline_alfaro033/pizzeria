<?php

use Illuminate\Database\Seeder;
use app\Cliente;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();
            for ($i=0; $i < 15; $i++) {
                  \DB::table("cliente_table_")->insert(
                        array(
                             'email'  => $faker->email,
                              'nombre'     => $faker->name,
                              'created_at' => date('Y-m-d H:m:s'),
                              'updated_at' => date('Y-m-d H:m:s')
                        )
                  );
            }
       
        }
   
}