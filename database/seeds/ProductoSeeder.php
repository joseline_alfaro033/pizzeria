<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    
    static $nomb =[
        
        array("nombre"=>'Personal de la casa', "precio"=>8),
        array("nombre"=>'Familiar de la casa', "precio"=>13),
        array("nombre"=>'Gigante de la casa', "precio"=>18),
        array("nombre"=>'Personal hawaiana', "precio"=>7),
        array("nombre"=>'Familiar hawaiana', "precio"=>12),
        array("nombre"=>'Gigante hawaiana', "precio"=>17),
        array("nombre"=>'Personal suprema', "precio"=>9),
        array("nombre"=>'Familiar suprema', "precio"=>14),
        array("nombre"=>'Gigante suprema', "precio"=>21),
        array("nombre"=>'Personal Personalizada', "precio"=>5),
        array("nombre"=>'Familiar Personalizada', "precio"=>8),
        array("nombre"=>'Gigante Personalizada', "precio"=>12),
    ];
        

    public function run()
    {
        foreach (self::$nomb as $nombr) {
      
                   DB::table('producto_table_')->insert([
                'nombre' => $nombr['nombre'],
                'precio'  => $nombr['precio'],
                'created_at' => date('Y-m-d H:m:s'),
                 'updated_at' => date('Y-m-d H:m:s')
            ]);
        
      }

       
        
    }
}
