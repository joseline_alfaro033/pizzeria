<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class IngredientesSeeder extends Seeder
{
      static $nomb =[
        
            array("nombre"=>'Peperoni', "precio"=>2),
        array("nombre"=>'Queso', "precio"=>1),
        array("nombre"=>'Jamon', "precio"=>2),
        array("nombre"=>'Piña', "precio"=>1),
        array("nombre"=>'Hongo', "precio"=>2),
        array("nombre"=>'Carne', "precio"=>4),
        array("nombre"=>'Tocino', "precio"=>2),
        array("nombre"=>'Cebolla', "precio"=>1),
        array("nombre"=>'Chile verde', "precio"=>1),
        array("nombre"=>'Aceitunas', "precio"=>1),
        array("nombre"=>'Salsa extra', "precio"=>2)];
 
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      foreach (self::$nomb as $nombr) {
      
                   DB::table('ingrediente_table_')->insert([
                        'nombre' => $nombr['nombre'],
                        'precio'  => $nombr['precio'],
                'created_at' => date('Y-m-d H:m:s'),
                 'updated_at' => date('Y-m-d H:m:s')
            ]);
        
      }


     
    }
}
