<?php

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            ClienteSeeder::class,
            SucursalSeeder::class,
            PedidoSeeder::class,
            IngredientesSeeder::class,
            ProductoSeeder::class,
            DetallePedidoSeeder::class,
            PizzaPersoSeeder::class,
            PizzaPredeSeeder::class,
        ]);
       

		
    }
}
