<?php


use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class SucursalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=0; $i < 10; $i++) {
              \DB::table("sucursal_table_")->insert(
                    array(
                         
                          'nombre'     => $faker->city,
                          'direccion'     => $faker->address,
                          'created_at' => date('Y-m-d H:m:s'),
                              'updated_at' => date('Y-m-d H:m:s')

                    )
              );
        }
   
    }
    
}
