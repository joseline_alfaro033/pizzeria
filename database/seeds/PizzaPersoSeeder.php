<?php
use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PizzaPersoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=0; $i < 5; $i++) {
              \DB::table("pizza_perso_table_")->insert(
                    array(
                          'id_detalle_pedido' => $this->getRandomDetallePId(),
                          
                          'id_ingrediente' => $this->getRandomIngredienteId(),
                         
                          'created_at' => date('Y-m-d H:m:s'),
                          'updated_at' => date('Y-m-d H:m:s')
                    )
              );
        }
   
    }
    private function getRandomDetallePId() {
        $dp = \App\DetallePedido::inRandomOrder()->first();
        return $dp->id;
    }
    private function getRandomIngredienteId() {
        $ingre = \App\Ingrediente::inRandomOrder()->first();
        return $ingre->id;
    }

}
