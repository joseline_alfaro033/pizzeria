<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetallePedidoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto_table_', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->String('nombre',100);
            $table->double('precio',100);
            $table->timestamps();
        });
        Schema::create('detalle_pedido_table_', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_pedido'); 
            $table->foreign('id_pedido')->references('id')->on('pedido_table_');
            $table->unsignedBigInteger('id_producto'); 
            $table->foreign('id_producto')->references('id')->on('producto_table_');
            
           $table->unsignedBigInteger('cantidad');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_pedido_table_');
        Schema::dropIfExists('producto_table_');
    }
}
