<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePizzaPersoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pizza_perso_table_', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_detalle_pedido'); 
            $table->foreign('id_detalle_pedido')->references('id')->on('detalle_pedido_table_');
            $table->unsignedBigInteger('id_ingrediente'); 
            $table->foreign('id_ingrediente')->references('id')->on('ingrediente_table_');
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pizza_perso_table_');
    }
}
