# Sistema de órdenes de pizzas

Herramientas utilizadas:
- php 7.3
- Laravel 7.x
- Xampp v3.2.4
- Composer version 1.10.10

Creacion de base de datos:
```sh
create database pizzeria;
```
Clonar repositorio:
```sh
git clone https://joseline_alfaro033@bitbucket.org/joseline_alfaro033/pizzeria.git
```
Ingresar en la carpeta y ejecutar:
```sh
$ composer install
```

Copiar archivo llamado .env.example y renombrar esta copia a .env
```sh
cp .env.example .env
```
# Creando un nuevo API key
Por medidas de seguridad cada proyecto de Laravel cuenta con una clave única que se crea en el archivo .env al iniciar el proyecto. Se puedes generar una nueva API key desde la consola usando:
```sh
$ php artisan key:generate
```

Posteriormente debes agregar las credenciales al archivo .env
```sh
DB_HOST=localhost
DB_DATABASE=tu_base_de_datos
DB_USERNAME=root
DB_PASSWORD=
```

Luego se ejecuta la migración con seeders 

```sh
 php artisan migrate --seed
```

Ahora, se inicia la aplicación utilizando el siguiente código:
```sh
 php artisan serve
```

# Se utilizó el programa Postman para hacer las siguientes pruebas:

- Consulta de combinaciones pre-establecidas de ingredientes.
```sh
GET http://127.0.0.1:8000/api/pizzaPredes
```

- Consulta de ingredientes disponibles para armar la pizza.
```sh
GET http://127.0.0.1:8000/api/ingredientes
```
- Ingresar el pedido:
    
```sh
POST http://127.0.0.1:8000/api/ipedido

Header: "Content-Type","application/json"

- Pizza preestablecida

  Petición:
{
"pedido":{
"detallePedido":[{"cantidad":1,"prod":{"id":1}},{"cantidad":2,"prod":{"id":2}}],
"sucursal":1,
"cliente":1
}
}

  -  Pizza personalizada
  Petición:
  {
"pedido":{
"detallePedido":[
{"cantidad":1,"prod":{"id":10,"ingredientes":[{"id":1},{"id":5}]}},

{"cantidad":2,"prod":{"id":11,"ingredientes":[{"id":4},{"id":2}]}}],
"sucursal":1,
"cliente":1
}
}
```
 - Consulta de mi historial de pedidos.

```sh
GET http://127.0.0.1:8000/api/historial/{id}
```
- Consulta de sucursales de la pizzería.
```sh
GET http://127.0.0.1:8000/api/sucursales
```
- Consulta de clientes más frecuentes.
```sh
GET http://127.0.0.1:8000/api/clienteFrecuente
```
- Consulta de clientes que más gastan ($) en la pizzería.
```sh
GET http://127.0.0.1:8000/api/clienteMasGasta
```
