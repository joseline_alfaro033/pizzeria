<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource("clientes","ClienteController");
Route::get('/historial/{id}',"ClienteController@historial");
Route::apiResource("detallePedidos","DetallePedidosController");
Route::apiResource("ingredientes","IngredienteController");
Route::apiResource("pedidos","PedidoController");
Route::apiResource("productos","ProductoController");
Route::apiResource("sucursales","SucursalController");
Route::apiResource("pizzaPredes","PizzaPredeController");
Route::post('/ipedido', "PedidoController@almacenarPedido");
Route::get('/clienteFrecuente',"ClienteController@clienteFrecuente");
Route::get('/clienteMasGasta',"ClienteController@clienteMasGasta");


